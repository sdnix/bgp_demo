import eventlet
import eventlet.debug
# BGPSpeaker needs sockets patched
eventlet.monkey_patch()
eventlet.debug.hub_prevent_multiple_readers(False)

import logging
from util import PrettyFormatter

LOG = logging.getLogger()
LOG.setLevel(logging.DEBUG)

l = logging.getLogger('bgpspeaker.cli')
l.setLevel(logging.INFO)

l = logging.getLogger('paramiko')
l.setLevel(logging.INFO)

fmt = PrettyFormatter('[%(levelname)5s] %(name)25s l.%(lineno)-4d')

ch = logging.StreamHandler()
ch.setFormatter(fmt)
LOG.addHandler(ch)

from ryu.services.protocols.bgp.bgpspeaker import BGPSpeaker
from ryu.services.protocols.bgp.info_base.ipv4 import Ipv4PrefixFilter
from ryu.services.protocols.bgp.info_base.ipv6 import Ipv6PrefixFilter
from ryu.services.protocols.bgp.operator.ssh import CONF, SshServer
from ryu.services.protocols.bgp.rtconf.neighbors import CONNECT_MODE_PASSIVE

CONF['ssh_host'] = '0.0.0.0'
SshServer.PROMPT = 'rs> '

if __name__ == "__main__":
    speaker = BGPSpeaker(as_number=64512, router_id='192.168.0.3',
                         ssh_console=True)
    speaker.neighbor_add('192.168.0.1', 64513, is_route_server_client=True,
                         connect_mode=CONNECT_MODE_PASSIVE)
    speaker.neighbor_add('192.168.0.2', 64514, is_route_server_client=True,
                         connect_mode=CONNECT_MODE_PASSIVE)
#    speaker.bmp_server_add('127.0.0.1', 11019)

    allv4 = Ipv4PrefixFilter('0.0.0.0/0', policy=Ipv4PrefixFilter.POLICY_DENY)
    allv6 = Ipv6PrefixFilter('0::/0', policy=Ipv6PrefixFilter.POLICY_DENY)

    speaker.in_filter_set('192.168.0.2', [allv4, allv6])
    speaker.out_filter_set('192.168.0.1', [allv4, allv6])

    eventlet.sleep(20)

    speaker.in_filter_set('192.168.0.2', [])

    eventlet.sleep(20)

    speaker.out_filter_set('192.168.0.1', [])

    while True:
        eventlet.sleep(40)
