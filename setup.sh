#!/bin/sh

cd `dirname $0`

docker rm -f ns01 ns02 rs

ns01=$(docker run -d -it --name ns01 -v `pwd`:/root/bgp_demo osrg/ryu python ./bgp_demo/ns01.py)
ns02=$(docker run -d -it --name ns02 -v `pwd`:/root/bgp_demo osrg/ryu python ./bgp_demo/ns02.py)
rs=$(docker run -d -it --name rs -v `pwd`:/root/bgp_demo osrg/ryu python ./bgp_demo/routeserver.py)

./pipework/pipework br1 ns01 192.168.0.1/24
./pipework/pipework br1 ns02 192.168.0.2/24
./pipework/pipework br1 rs 192.168.0.3/24
