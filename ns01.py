import eventlet
import eventlet.debug
# BGPSpeaker needs sockets patched
eventlet.monkey_patch()
eventlet.debug.hub_prevent_multiple_readers(False)

# initialize a log handler
# this is not strictly necessary but useful if you get messages like:
#    No handlers could be found for logger "ryu.lib.hub"
import logging
from util import PrettyFormatter

LOG = logging.getLogger()
LOG.setLevel(logging.DEBUG)

l = logging.getLogger('bgpspeaker.cli')
l.setLevel(logging.INFO)

l = logging.getLogger('paramiko')
l.setLevel(logging.INFO)

fmt = PrettyFormatter('[%(levelname)5s] %(name)25s l.%(lineno)-4d')

ch = logging.StreamHandler()
ch.setFormatter(fmt)
LOG.addHandler(ch)



from ryu.services.protocols.bgp.bgpspeaker import BGPSpeaker
from ryu.services.protocols.bgp.info_base.base import PrefixFilter
from ryu.services.protocols.bgp.operator.ssh import CONF, SshServer

CONF['ssh_host'] = '0.0.0.0'
SshServer.PROMPT = 'ns01> '

if __name__ == "__main__":
    speaker = BGPSpeaker(as_number=64513, router_id='192.168.0.1',
                         ssh_console=True)
    speaker.neighbor_add('192.168.0.3', 64512)
#    speaker.bmp_server_add('127.0.0.1', 11019)

    while True:
        eventlet.sleep(40)
