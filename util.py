import logging
from curses import setupterm, tigetnum
import os

def get_terminal_size():
    rows, cols = os.popen('stty size', 'r').read().split()
    return int(cols), int(rows)

class PrettyFormatter(logging.Formatter):
    def format(self, record):
        record.message = record.getMessage()
        if self.usesTime():
            record.asctime = self.formatTime(record, self.datefmt)

        cols, _ = get_terminal_size()
        header = self._fmt % record.__dict__
        separator = ' | '
        max_body_len = cols - len(header + separator)

        if max_body_len < 10:
            return 'terminal is too small.'

        lines = []
        for line in record.message.split('\n'):
            while len(line) > 0:
                lines.append(line[:max_body_len])
                line = line[max_body_len:]

        s = ''
        for i, line in enumerate(lines):
            if i == 0:
                s += header + separator
            else:
                s += '\n' + ' ' * len(header) + separator
            s += line
        return s
